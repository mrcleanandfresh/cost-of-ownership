<?php

use Ownership\Ownership;
use Symfony\Component\HttpFoundation\Request;

require_once __DIR__ . '/../vendor/autoload.php';

$app = new Silex\Application();

$app->post( '/ownership/cost/{thing}', function ( $thing, Request $request ) use ( $app ) {

	$currOdo       = $request->get( 'currOdo' );
	$prevOdo       = $request->get( 'prevOdo' );
	$fuelGallons   = $request->get( 'fuelGallons' );
	$fuelCost      = $request->get( 'fuelCost' );
	$fuelCapacity  = $request->get( 'fuelCapacity' );
	$otherExpenses = $request->get( 'otherExpenses' ) !== null ? $request->get( 'otherExpenses' ) : array();
	$tireTreadLife = $request->get( 'tireTreadLife' );
	$tireCost      = $request->get( 'tireCost' );
	$tireNumber    = $request->get( 'tireNumber' );

	$ownership = new Ownership();
	if ( $thing == strtolower( 'car' ) ) {
		try {
			$cost = $ownership::findOwnershipCostForCar( $currOdo, $prevOdo, $fuelGallons, $fuelCost, $fuelCapacity, $otherExpenses, $tireTreadLife, $tireCost, $tireNumber );
		} catch ( Exception $e ) {
			return $app->json( array(
				"error"   => $e->getCode(),
				"message" => $e->getMessage()
			), 500 );
		}
	} else {
		$cost = array();
	}

	return $app->json( $cost );
});

$app->run();