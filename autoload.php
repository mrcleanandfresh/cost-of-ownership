<?php
$loader = require __DIR__ . '/vendor/autoload.php';
$loader->addPsr4( 'Ownership\\UnitTest\\', __DIR__ . '/src/unit-tests' );
$loader->addPsr4( 'Ownership\\IntegrationTest\\', __DIR__ . '/src/integration-tests' );