<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 7/6/2017
 * Time: 10:53 AM
 */

namespace Ownership\Cost;

use Ownership\Calculate\CostsCalculator;
use Ownership\Calculate\FuelCalculator;
use Ownership\Calculate\TiresCalculator;

class OwnershipCarCost implements CarCost {
	private $_currentOdometer;
	private $_previousOdometer;

	public function __construct( $currOdo, $prevOdo ) {
		$this->_currentOdometer  = $currOdo;
		$this->_previousOdometer = $prevOdo;
	}

	function costPerMileToOperate( $fuelGallons, $fuelCost, $fuelCapacity, array $otherExpenses, $tireTreadLife, $tireCost, $tireNumber ) {
		$costPerMile = 0.0;
		$costPerMile += $this->costOfFuel( $fuelGallons, $fuelCost, $fuelCapacity );
		$costPerMile += $this->costOfExpenses( $otherExpenses );
		$costPerMile += $this->costOfTires( $tireTreadLife, $tireCost, $tireNumber );

		$costs = array(
			'costOfFuel'       => $this->costOfFuel( $fuelGallons, $fuelCost, $fuelCapacity ),
			'costOfExpenses'   => $this->costOfExpenses( $otherExpenses ),
			'costOfTires'      => $this->costOfTires( $tireTreadLife, $tireCost, $tireNumber ),
			'totalCostPerMile' => $costPerMile
		);

		return $costs;
	}

	function costOfFuel( $gallons, $costOfFuel, $fuelCapacity ) {
		$fuel = new FuelCalculator( $this->_currentOdometer, $this->_previousOdometer, $gallons, $costOfFuel, $fuelCapacity );

		return $fuel->getCostPerMile();
	}

	function costOfExpenses( array $expenses ) {
		$ve = new CostsCalculator( $expenses, $this->_currentOdometer - $this->_previousOdometer );

		return $ve->getCostsPerMile();
	}

	function costOfTires( $treadLife, $costPerTire, $numTires ) {
		$tires = new TiresCalculator( $treadLife, $costPerTire, $numTires );

		return $tires->getCostPerMile();
	}
}