<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 7/6/2017
 * Time: 2:48 PM
 */

namespace Ownership\Calculate;


trait Costs {
	protected static function determineExpenses( $expenses ) {
		$totalExpenses = 0.0;
		if ( ! empty( $expenses ) ) {
			while ( current( $expenses ) !== false ) {
				$totalExpenses += Calculator::sanitizeNumForCalc( current( $expenses ) );
				next( $expenses );
			}
		}

		return $totalExpenses;
	}
}