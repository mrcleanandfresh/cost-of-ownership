<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 7/6/2017
 * Time: 1:18 PM
 */

namespace Ownership\Calculate;


trait Calculator {
	public static function sanitizeNumForCalc( $num ) {
		$detectCommas  = '/(?<=\d),(?=\d)/';
		$detectDecimal = "/(\\d)\\.+/";

		$cleanNumber = ( preg_match( $detectCommas, $num ) ) ?
			preg_replace( $detectCommas, '', $num )
			: $num;

		if ( preg_match( $detectDecimal, $cleanNumber ) ) {
			return floatval( $cleanNumber );
		} else {
			return intval( $cleanNumber );
		}
	}

	protected static function handleFloatedCost( $cost ) {
		if ( is_float( $cost ) ) {
			return round( $cost, 2 );
		}

		return floatval( sprintf( "%.2f", $cost ) );
	}
}