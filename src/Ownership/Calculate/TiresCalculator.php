<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 7/6/2017
 * Time: 11:11 AM
 */

namespace Ownership\Calculate;


/**
 * Class TiresCalculator
 * @package Ownership\Calculate
 */
class TiresCalculator {
	use Calculator;

	/** @var int $_treadLife */
	private $_treadLife;
	/** @var float $_cost */
	private $_cost;
	/** @var int $_numberOfTires */
	private $_numberOfTires;

	/**
	 * TiresCalculator constructor.
	 *
	 * @param int   $mileageWarranty
	 * @param float $costPerTire
	 * @param int   $numTires
	 */
	public function __construct( $mileageWarranty, $costPerTire, $numTires ) {
		$this->_treadLife     = $this::sanitizeNumForCalc( $mileageWarranty );
		$this->_cost          = $this::sanitizeNumForCalc( $costPerTire );
		$this->_numberOfTires = $this::sanitizeNumForCalc( $numTires );
	}

	/**
	 * @return float
	 */
	public function getCostPerMile() {
		if ( $this->_getTotalCost() !== null && $this->_getTotalCost() !== 0 ) {
			return $this->handleFloatedCost( $this->_getTotalCost() / $this->_treadLife );
		} else {
			return 0;
		}
	}

	/**
	 * @return float
	 */
	private function _getTotalCost() {
		return $this->_cost * $this->_numberOfTires;
	}

	/**
	 * @return int
	 */
	public function getTreadLife() {
		return $this->_treadLife;
	}

	/**
	 * @return float
	 */
	public function getCost() {
		return $this->_cost;
	}

	/**
	 * @return int
	 */
	public function getNumberOfTires() {
		return $this->_numberOfTires;
	}

}