<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 7/6/2017
 * Time: 11:11 AM
 */

namespace Ownership\Calculate;


class CostsCalculator {
	use Costs;
	use Calculator;

	/** @var  float $_totalExpenses */
	private static $_totalExpenses = 0.0;
	/** @var  array $_expenses */
	private $_expenses;
	/** @var  float $_monthlyMileage */
	private $_monthlyMileage;

	/**
	 * MaintenanceCalculator constructor.
	 *
	 * @param array $expenses
	 * @param float $monthlyMileage
	 */
	public function __construct( array $expenses, $monthlyMileage ) {
		$this->_expenses       = $expenses;
		$this->_monthlyMileage = $this->sanitizeNumForCalc( $monthlyMileage );
		$this::$_totalExpenses = $this::determineExpenses( $expenses );
	}

	public function getCostsPerMile() {
		// make sure we aren't dividing by zero
		if ( $this::$_totalExpenses !== null && $this::$_totalExpenses !== 0.0 ) {
			$expenses = $this::$_totalExpenses / $this->_monthlyMileage;
		} else {
			throw new \Exception( "Please enter a mileage other than zero." );
		}

		return $this->handleFloatedCost( $expenses );
	}
}