<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 7/10/2017
 * Time: 9:42 AM
 */

namespace Ownership\Factory;


use Ownership\Cost\OwnershipCarCost;

class OwnershipCarCostFactory {
	public static final function getOwnershipCarCost( $currOdo, $prevOdo ) {
		return new OwnershipCarCost( $currOdo, $prevOdo );
	}
}