<?php

namespace Ownership\UnitTest\Calculate;

use Ownership\Calculate\CostsCalculator;
use Ownership\Calculate\FixedCostsCalculator;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 7/6/2017
 * Time: 2:59 PM
 */
class CostsCalculatorTest extends TestCase {
	/** @var  CostsCalculator $costsCalculator */
	public $costsCalculator;
	public $expenses = [
		350.75,
		176.33,
		123.45,
		796.32,
		44.5,
		81.33
	];

	public function setUp() {
		$this->costsCalculator = new CostsCalculator( $this->expenses, 1300.50 );
	}

	public function testConstructorDeterminesExpenses() {
		$reflectedFixedCostsCalc = new \ReflectionClass( $this->costsCalculator );
		$actual                  = $reflectedFixedCostsCalc->getProperty( '_totalExpenses' );
		$actual->setAccessible( true );
		$this->assertTrue( $actual->getValue() !== 0, "The total expenses should have been set in the constructor." );
	}

	public function testGettingFixedCostsPerMile() {
		$expectedExpenses = 0.0;
		$expensesCount    = count( $this->expenses );
		for ( $i = 0; $i < $expensesCount; ++ $i ) {
			$expectedExpenses += $this->expenses[ $i ];
		}
		// uses rounding internally before returning
		$expected = round( $expectedExpenses / 1300.50, 2 );
		$actual   = $this->costsCalculator->getCostsPerMile();

		$this->assertEquals( $expected, $actual, "The fixed costs per mile should give us back expenses divided by miles." );
	}
}