<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 7/6/2017
 * Time: 3:03 PM
 */

namespace Ownership\UnitTest\Calculate;

use Ownership\Calculate\Calculator;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase {
	use Calculator;

	public function testHandleFloatedCost() {
		$num      = 500.33;
		$expected = $this->handleFloatedCost( $num );
		$this->assertTrue( is_float( $expected ) );
		$num      = 500;
		$expected = $this->handleFloatedCost( $num );
		$this->assertTrue( is_float( $expected ) );
	}

	public function testStripCommaFromNumIfThereIsComma() {
		$test     = "1,000,000.004532";
		$expected = 1000000.004532;
		$actual   = $this->sanitizeNumForCalc( $test );

		$this->assertTrue( is_float( $expected ) );
		$this->assertTrue( is_float( $actual ) );
		$this->assertEquals( $expected, $actual, "The regex should strip the commas out before the decimal place." );
	}

	public function testStripCommaFromNumSkipsIfNoCommas() {
		$test     = "1000000.004532";
		$expected = 1000000.004532;
		$actual   = $this->sanitizeNumForCalc( $test );

		$this->assertTrue( is_float( $expected ) );
		$this->assertTrue( is_float( $actual ) );
		$this->assertEquals( $expected, $actual, "The regex should strip the commas out before the decimal place." );
	}
}