<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 7/6/2017
 * Time: 3:00 PM
 */

namespace Ownership\UnitTest\Calculate;


use Ownership\Calculate\TiresCalculator;
use PHPUnit\Framework\TestCase;

class TiresCalculatorTest extends TestCase {
	/** @var  TiresCalculator $tiresCalculator */
	public $tiresCalculator;

	public function setUp() {
		$this->tiresCalculator = new TiresCalculator( 60000, 465, 4 );
	}

	public function testGettingCostPerMile() {
		$expected = round( ( $this->tiresCalculator->getCost() * $this->tiresCalculator->getNumberOfTires() ) / $this->tiresCalculator->getTreadLife(), 2 );
		$actual   = $this->tiresCalculator->getCostPerMile();
		$this->assertEquals( $expected, $actual, "The cost of the tires should be the total cost of the tires divided by the tread life." );
	}
}