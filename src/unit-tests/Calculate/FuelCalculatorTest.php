<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 7/6/2017
 * Time: 3:00 PM
 */

namespace Ownership\UnitTest\Calculate;


use Ownership\Calculate\FuelCalculator;
use PHPUnit\Framework\TestCase;

class FuelCalculatorTest extends TestCase {
	/** @var  FuelCalculator $fuelCalculator */
	public $fuelCalculator;

	public function setUp() {
		$this->fuelCalculator = new FuelCalculator( 94370, 94087, 14.564, 2.239, 19 );
	}

	public function testGetMilesPerGallon() {
		$miles    = $this->fuelCalculator->getCurrentOdometer() - $this->fuelCalculator->getPreviousOdometer();
		$expected = round( ( $miles / $this->fuelCalculator->getGallonsAdded() ) * 100, 2 ) / 100;
		$actual   = $this->fuelCalculator->getMilesPerGallon();
		$this->assertEquals( $expected, $actual );
	}

	public function testGetCostPerMile() {
		$miles    = $this->fuelCalculator->getCurrentOdometer() - $this->fuelCalculator->getPreviousOdometer();
		$mpg      = round( ( $miles / $this->fuelCalculator->getGallonsAdded() ) * 100, 2 ) / 100;
		$expected = round( ( $this->fuelCalculator->getFuelCost() / $mpg ) * 100, 2 ) / 100;
		$actual   = $this->fuelCalculator->getCostPerMile();
		$this->assertEquals( $expected, $actual );
	}
}