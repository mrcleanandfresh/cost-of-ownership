<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 7/6/2017
 * Time: 3:03 PM
 */

namespace Ownership\UnitTest\Calculate;

use Ownership\Calculate\Costs;
use PHPUnit\Framework\TestCase;

class CostsTest extends TestCase {
	use Costs;

	public function testDetermineExpensesAddsNumbers() {
		$expenses = [
			250,
			321.33,
			32.34,
			663.3
		];
		$expected = 0.0;
		for ( $i = 0; $i < count( $expenses ); $i ++ ) {
			$expected += $expenses[ $i ];
		}

		$actual = $this->determineExpenses( $expenses );

		$this->assertEquals( $expected, $actual, "Determine Expenses should deliver a number as if all expenses are added together." );
	}
}